const mongoose = require('mongoose')
const { Schema } = mongoose;

const vegetableSchema = new Schema({
  name: String,
  consumers: Number,

});

module.exports = vegetableSchema;