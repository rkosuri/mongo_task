const mongoose = require('mongoose');

const { vegetableSchema } = require('../schema');

const vegetable = mongoose.model('vegetable', vegetableSchema);

module.exports = vegetable;