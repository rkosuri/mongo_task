// external stufff
const express = require("express");
const mongoose = require("mongoose");

// internal stuff
const vegetable = require("./database/model");
const { MONGO_URL, PORT } = require("./constants");

// creating and defining an Express application
const app = express();

app.get("/", (req, res) => {

	vegetable.find((err, data) => {
		if (!err) {
			res.send(data);
		} else {
			res.status(400).send(err);
		}
	});
});

app.post("/", (req, res) => {
	vegetable.create([{ name: "potato" , consumer:"10" }]);
});

// app.put("/", (req, res) => {
//   res.send("");
// });

// app.delete("/", (req, res) => {
//   res.send("");
// });

// connecting to a MongoDB instance
mongoose.connect(
	MONGO_URL,
	() => {  // on success of connectionl try listening on PORT
		app.listen(PORT, () => {
			console.log(
				"Database connection is Ready and " + "Server is Listening on Port ", PORT);
		});
	})